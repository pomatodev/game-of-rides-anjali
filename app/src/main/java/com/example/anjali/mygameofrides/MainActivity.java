package com.example.anjali.mygameofrides;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {

    private ProgressDialog progressDialog;
    private MessageHandler messageHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Counting");
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(true);
        messageHandler = new MessageHandler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doNotify() {
        Intent intent = new Intent(this, enquiry_kingdom.class);
        startActivity(intent);
    }


    public class MyActivity extends ActionBarActivity {
        public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";
    }

    public class Timer implements Runnable {

        @Override
        public void run() {

            for (int i = 10; i >= 0; i--) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {

                }

                Bundle bundle = new Bundle();
                bundle.putInt("current count", i);

                Message message = new Message();
                message.setData(bundle);

                messageHandler.sendMessage(message);


            }

            progressDialog.dismiss();

        }


    }

    public void startCounter(View v) {

        progressDialog.show();

        Thread thread = new Thread(new Timer());
        thread.start();

    }


    private class MessageHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message message) {
            int currentCount = message.getData().getInt("current count");
            progressDialog.setMessage("Next page starts in..." + currentCount);

            if (currentCount == 0) {
                doNotify();

            }
        }

    }
}

